#!/bin/bash
cd /Users/kosta/git/neuralnetworkproject/neuralNetworkProject
rm -rf dist
rm -rf build
python setup.py py2app -r data,picToCsv.sh --iconfile ../resource/neuron.icns
cd dist
cd *.app/Contents/Resources
cp -R /opt/local/lib/Resources/qt_menu.nib .
touch qt.conf
#sed -i "s/'macosx_app'.*\n /'macosx_app'\n    sys.path = [os.path.join(os.environ['RESOURCEPATH'], 'lib', 'python2.7', 'lib-dynload')] + sys.path + 
#[os.path.join(os.environ['RESOURCEPATH'], 'lib', 'python2.7', 'scipy')] + [os.path.join(os.environ['RESOURCEPATH'], 'lib', 'python2.7')]/" __boot__.py
### Conact manager specific fix
# install my own Info.plist
cd ..
cp ../../../../resource/Info.plist .
cd ../.. 
mv main.app NSA\ Project.app

