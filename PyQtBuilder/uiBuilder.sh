#!/bin/bash
CODEDIR="neuralNetworkProject"
for i in $(ls) 
do
    echo $PYTHONPATH
    if [ ${i: -3} == ".ui" ]
    then
    echo "compiling python UI code for $i"
    NAME=$(basename $i .ui)
    NAME="${NAME}UI.py"
    pyuic $i > $NAME
    mv $NAME ../$CODEDIR/
    echo "done for $i"
    fi
done