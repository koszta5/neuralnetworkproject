'''
Created on Apr 25, 2012

@author: Pavel Kostelnik
'''
from sampleDB import SampleDB
from csvDataLoader import CsvDataLoader
from window import Window
from mySteam import MyStream
import sys
class Main(object):
    '''
    classdocs
    '''
    def main(self):
        self.stream = MyStream(sys.stdout)
        self.stream.start()
        self.db = SampleDB()
        self.dataLoader = CsvDataLoader(self.db)
        self.window = Window(self)
        
  

    def __init__(self):
        '''
        Constructor
        '''
        
if __name__ == "__main__":
    app = Main()
    app.main()