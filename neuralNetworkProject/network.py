'''
Created on Apr 25, 2012

@author: Pavel Kostelnik
'''
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers.backprop import BackpropTrainer
from PyQt4.QtCore import QThread
from PyQt4 import QtCore
from pybrain.tools.validation import CrossValidator
class Network(QThread):
    '''
    classdocs
    '''
    def cross_validate(self):
        print "starting to crossvalidate data with 10 folds"
        self.crossvalidator = CrossValidator(self.trainer, self.sample_DBobj.data_set, n_folds = 10)
        self.crossvalidator.validate()
        self.emit(QtCore.SIGNAL("crossvalidated()"))
    
    def train(self):
        print "Starting to train network!!! -- Give it some time (takes about 7 mins on Core2Duo 2.4 GHz with default settings)"
        self.trainer.trainUntilConvergence(maxEpochs=10, validationProportion=0.25,continueEpochs=3)
        self.trainer.testOnData(verbose=True)
        self.emit(QtCore.SIGNAL("learningNetworkDone()"))
        
    def test_single_data(self, dataSample):
        return self.net.activate(dataSample)

    def __init__(self, sampleDB):
        '''
        Constructor
        '''
        QThread.__init__(self)
        self.sample_DBobj = sampleDB
        self.net = buildNetwork(1892,50,12,1, bias=True)
        self.trainer = BackpropTrainer(self.net, self.sample_DBobj.data_set, learningrate=0.01,momentum=0.4,verbose=True)

    def run(self):
        self.train()