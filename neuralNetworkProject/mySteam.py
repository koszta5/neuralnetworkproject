'''
Created on Apr 25, 2012

@author: Pavel Kostelnik
'''
from StringIO import StringIO
from PyQt4.QtCore import QThread
from PyQt4 import QtCore
import sys
class MyStream(StringIO, QThread):
    '''
    classdocs
    '''


    def __init__(self, sysstdout):
        '''
        Constructor
        '''
        StringIO.__init__(self)
        QThread.__init__(self)
        self.backup_std_out = sysstdout
        sys.stdout = self
        
        
        
    def write(self, text):
        print >> self.backup_std_out, text
        self.emit(QtCore.SIGNAL("textAdded"), text)
        
    def run(self):
        pass