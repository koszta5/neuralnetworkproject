'''
Created on Apr 25, 2012

@author: Pavel Kostelnik
'''
from PyQt4 import QtCore, QtGui
from mainWindowUI import Ui_Letterclasifier
import sys,os
from network import Network
class Window(QtGui.QMainWindow):
    '''
    classdocs
    '''


    def __init__(self, main):
        '''
        Constructor
        '''
        self.main = main
        self.alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        self.createApp()
        QtGui.QMainWindow.__init__(self)
        self._ui = Ui_Letterclasifier()
        self._ui.setupUi(self)
        self.connect_signals()
        self.show()
        self.raise_()
        self.run()
        
        
        
    def createApp(self):
        """
        Creates a Qtapp that is necessary for the program to run
        """
        self._QtApp = QtGui.QApplication(sys.argv)
    
    def run(self):
        """
        Make app exitable
        """
        sys.exit(self._QtApp.exec_())
    
    def add_msg(self, text):
        self._ui.stdOutTextEdit.appendPlainText(text)
    
    def connect_signals(self):
        self._ui.stdOutTextEdit.setReadOnly(True)
        self.connect(self._ui.trainBtn, QtCore.SIGNAL("clicked()"), self.load_data)
        self.connect(self.main.stream, QtCore.SIGNAL("textAdded"), self.add_msg)
        self.connect(self.main.dataLoader, QtCore.SIGNAL("dataLoaderDone()"), self.build_network)
        self.connect(self._ui.changeDataSetBtn, QtCore.SIGNAL("clicked()"), self.choose_default_dataset_file)

        
        
    def load_data(self):
        self.main.dataLoader.start()
        
    def build_network(self):
        self.main.net = Network(self.main.db)
        self.main.net.start()
        self._ui.statusbar.showMessage("Network learning started, learning, be patient...")
        self.connect(self.main.net, QtCore.SIGNAL("learningNetworkDone()"), self.allow_new_sample)
        
    def need_to_crossvalidate(self):
        self.main.net.cross_validate()
            
    def crossvalidation_done(self):
        self._ui.statusbar.showMessage("Network cross validation finished", 4000)
    
    def allow_new_sample(self):
        self._ui.statusbar.showMessage("Network has finished learning", 5000)
        self.connect(self.main.net, QtCore.SIGNAL("crossvalidated()"), self.crossvalidation_done)
        self._ui.crossvalidateBtn.setEnabled(True)
        self.enable_widgets()
        self.disable_retrain()
        self.connect_new_signals()
        
    def choose_default_dataset_file(self):
        new_data_set = QtGui.QFileDialog.getOpenFileName(self, 'Open data sample File', os.getenv("HOME"))
        if new_data_set:
            self.main.dataLoader._source_file = new_data_set
            self._ui.statusbar.showMessage("Changed default dataset file to " + str(self.main.dataLoader._source_file), 4000)

        
    def disable_retrain(self):
        self._ui.trainBtn.setEnabled(False)
        self._ui.changeDataSetBtn.setEnabled(False)
    
    def connect_new_signals(self):
        self.connect(self._ui.classifyNewDataBtn, QtCore.SIGNAL("clicked()"), self.classify_new_data)
        self.connect(self._ui.crossvalidateBtn, QtCore.SIGNAL("clicked()"), self.need_to_crossvalidate)
        self.connect(self._ui.newDataFromImageBtn, QtCore.SIGNAL("clicked()"), self.new_data_from_image)
        
    def new_data_from_image(self):
        self._ui.statusbar.showMessage("Generating csv from your picture...")
        pic_file = str(QtGui.QFileDialog.getOpenFileName(self, 'Open data sample file', os.getenv("HOME")))
        os.system("/bin/bash ./picToCsv.sh "+pic_file)
        sample_file = os.getenv("HOME") + os.sep + "sample.txt"
        f = open(sample_file, "w+")
        sample = f.readlines()[0]
        f.close()
        self._ui.newDataTextEdit.setPlainText(sample)
        self.classify_new_data()
        

        
    def enable_widgets(self):
        self._ui.newDataDumpLbl.setEnabled(True)
        self._ui.newDataTextEdit.setEnabled(True)
        self._ui.newDataFromImageBtn.setEnabled(True)
        self._ui.classifyNewDataBtn.setEnabled(True)
        self._ui.newDataClassifiedAsLbl.setEnabled(True)
        
    def classify_new_data(self):
        self._ui.statusbar.showMessage("Classifiying data ...", 25000)
        text = str(self._ui.newDataTextEdit.toPlainText())
        test_data, test_result = self.main.dataLoader.parse_line(text)
        result = self.main.net.test_single_data(test_data)
        result = result[0]
        print "classifier output of test sample is ---> " + str(result)
        letter = self.convert_to_letter(result)
        self._ui.newDataLbl.setText(letter)
        
    def convert_to_letter(self, number):
        number = round(number, 0)
        number = int(number)
        try:
            letter = self.alphabet[number-1]
        except IndexError,e:
            letter = "Error"
        return letter
        
        