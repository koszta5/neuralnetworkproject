'''
Created on Apr 25, 2012

@author: Pavel Kostelnik
'''
import os
from PyQt4.QtCore import QThread
from PyQt4 import QtCore
class CsvDataLoader(QThread):
    '''
    classdocs
    '''


    def __init__(self, db):
        '''
        Constructor
        '''
        QThread.__init__(self)
        self._source_file = os.path.abspath(os.path.join(os.curdir)) +os.sep+ "data" + os.sep+ "seznam_matlab-octave.csv"
        self.sample_DBobj = db
        
    def parse_line(self, line):
        line  = line.rstrip().lstrip()
        test_result = ""
        samples = line.split(",")
        test_data = samples[0]
        try:
            test_result = samples[1]
        except IndexError,e:
            pass
        try:
            test_result = float(test_result)
        except ValueError,e:
            pass
        test_data = map(float, test_data)
        test_data = tuple(test_data)
        return test_data, test_result
    
    def load_letter_data(self):
        count = self.count_samples()
        percent = count / 100
        f = open(self._source_file)
        line = f.readline()
        line_count = 0
        while line:
            line_count = line_count + 1
            test_data, test_result = self.parse_line(line)
            self.sample_DBobj.data_set.addSample(test_data, (test_result,))
            if (line_count%percent == 0):
                string =  "loading data --> "+str(line_count/percent) + "% done"
                print string
            line = f.readline()
        f.close()
        print "All data samples loaded!!!"
        self.emit(QtCore.SIGNAL("dataLoaderDone()"))
    
    def count_samples(self):
        with open(self._source_file) as f:
            for i, l in enumerate(f):
                pass
            return i + 1
        
        
    def run(self):
        self.load_letter_data()